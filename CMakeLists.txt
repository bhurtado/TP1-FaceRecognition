cmake_minimum_required(VERSION 3.0)
project(face_recognition_system)

find_package( OpenCV REQUIRED )
find_package(Boost REQUIRED COMPONENTS system filesystem)
include_directories( ${Boost_INCLUDE_DIRS} )
include_directories(/usr/include/cppconn)
include_directories(../include)
set(CMAKE_BUILD_TYPE Debug)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14")

add_executable(face_recognition_system
	../src/main.cpp
	../src/func.cpp
	../src/sistema.cpp
	../src/face_rec.cpp)
target_link_libraries( face_recognition_system ${Boost_FILESYSTEM_LIBRARY} ${Boost_SYSTEM_LIBRARY})
target_link_libraries(face_recognition_system ${OpenCV_LIBS} )
target_link_libraries(face_recognition_system  mysqlcppconn)
