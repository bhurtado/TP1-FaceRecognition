/*! @file sistema.cpp
*	@authors Guilherme Lopes and Alex Souza
*	@brief Esse arquivo descreve as funções de cadastro do sistema de reconhecimento facial.
*/
//C++ REQUIRED LIBRARIES
#include <iostream>
#include <ostream>
#include <string>
#include <vector>
//OPENCV REQUIRED LIBRARIES 
#include <opencv2/opencv.hpp>
#include <opencv/highgui.h>
//BOOST FILESYSTEM LIBRARIES
#include <boost/filesystem.hpp>
//MYSQL REQUIRED LIBRARIES 
#include "mysql_connection.h"
#include <mysql_driver.h>
#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>
#include <cppconn/prepared_statement.h>
//HEADERS
#include "sistema.hpp"
#include "faces.hpp"

using namespace std;
using namespace sql;
using namespace cv;

/** \brief Função responsável por orientar como utilizar o sistema*/
void usage()
{
	cout << "Forma de execucao" << endl;
	cout <<  "./program <path/to/project/folder>" << endl;
}

/** \brief Função responsável por cadastrar o usuário*/
void cadastraUsuario(int opcao, PreparedStatement* pstmt, Driver* driver, Connection* con, Statement* stmt, ResultSet* res){

	/**
	 \details Cadastra um novo usuario no sistema armazenando seus dados principais
	 \e coletando fotos para seu reconhecimento.
	 \param pstmt é a variável responsável por fazer uma Query no mySQL
	 \param driver é a variável responsável por instanciar um driver
	 \param con é responsável pela conexão com o DB
	 \param stmt é responsável por fazer execuções no DB
	 \param res é utilizado para armazenar a resposta à Query
	*/
	string path, nome, senha, sobrenome, diretorio, disciplina, cod_turma, matricula_string;
	ostringstream convert;
	int matricula, num_mat, i=0; //Número de materias de cada aluno

	if(con->isValid()){
		cout << "Digite o nome: ";
		cin >> nome;
		cout << "Digite o sobrenome: ";
		cin >> sobrenome;
		cout << "Digite o senha: ";
		cin >> senha;
		
		if(opcao == 1 || opcao == 2)
		{
			cout << "Digite o numero de disciplinas: ";
			cin >> num_mat;

			while(i<num_mat){
				cout << "Digite o nome da disciplina: ";
				cin >> disciplina;
				
				 /* Verifica se existe a disciplina */
				 pstmt = con->prepareStatement("SELECT nome FROM DISCIPLINA WHERE DISCIPLINA.nome = ?");
				 pstmt->setString(1,disciplina);
				 res = pstmt->executeQuery();

				if(res->rowsCount() == 1){
					cout << "Digite a turma: ";
					cin >> cod_turma;
					i++;
					if(opcao == 1)
					{
						pstmt = con->prepareStatement("INSERT INTO CADASTRO_DISCIPLINA (nome,matricula_aluno,turma) VALUES(?,?,?)");
						pstmt->setString(1,disciplina);
						pstmt->setInt(2,matricula);
						pstmt->setString(3,cod_turma);
						pstmt->executeUpdate();
					}
					else
					{
						pstmt = con->prepareStatement("INSERT INTO DISCIPLINA (matricula_professor,nome,turma) VALUES(?,?,?)");
						pstmt->setInt(1,matricula); //Ajustar o cadastro de disciplina
						pstmt->setString(2,nome);
						pstmt->setString(3,cod_turma);
						pstmt->executeUpdate();
					}
				} else cout << "Disciplina não existe no sistema!" << endl;
			}
			cout << "Digite a matricula:";
			cin >> matricula;
		} 
		else
		{
			cout << "Digite o CPF:";
			cin >> matricula;
		}
		if(opcao == 1)
		{
			path = "../Aluno/";
			pstmt = con->prepareStatement("INSERT INTO ALUNO (matricula,senha,nome,sobrenome) VALUES(?,?,?,?)");
		}
		else if(opcao == 2)
		{
			path = "../Professor/";
			pstmt = con->prepareStatement("INSERT INTO PROFESSOR (matricula,senha,nome,sobrenome) VALUES(?,?,?,?)");
		}
		else
		{
			path = "../Funcionario/";
			pstmt = con->prepareStatement("INSERT INTO FUNCIONARIO (cpf,senha,nome,sobrenome) VALUES(?,?,?,?)");
		}
		pstmt->setInt(1,matricula); 
		pstmt->setString(2,senha);
		pstmt->setString(3,nome);
		pstmt->setString(4,sobrenome);
		pstmt->executeUpdate();

		convert << matricula;
		matricula_string = convert.str();
		diretorio = path + matricula_string;	
		/*Filesystem Library possui a função de criar um diretorio */
		/* Sendo assim, para cada matrícula, cria-se uma pasta*/
		boost::filesystem::path dir(diretorio);
		if(boost::filesystem::create_directory(dir)) {
			cout << "Cadastrado Com Sucesso" << endl;
		}

		set_camera_print(matricula_string, diretorio);
		delete pstmt;
	}
}
/** \brief Função responsável por organizar qual o tipo de cadastro será feito para cada pessoa, seja aluna, funcionaria ou professora.*/
void cadastraReserva(PreparedStatement* pstmt, Driver* driver, Connection* con, Statement* stmt, ResultSet* res){
	/**
	 \details Cadastra uma nova reserva no sistema armazenando as principais informações no banco de dados.
	 \param pstmt é a variável responsável por fazer uma Query no mySQL
	 \param driver é a variável responsável por instanciar um driver
	 \param con é responsável pela conexão com o DB
	 \param stmt é responsável por fazer execuções no DB
	 \param res é utilizado para armazenar a resposta à Query
	*/
	int opcao, usuario;
	if(con->isValid()){
		while(opcao != 5){
			cout << "Menu Opções" << endl;
			cout << "1 - Aluno" << endl;
			cout << "2 - Professor" << endl;
			cout << "3 - Funcionario" << endl;
			cout << "4 - Sair" << endl;
			cout << "Digite a opção desejada:" << endl;
			cin >> opcao;

			if(opcao > 4 || opcao < 1)
			{
				cout << "Digite uma opcao valida"	 << endl;
				cin >> opcao;
			}
			switch(opcao)
			{
				case 1:
					cout << "Digite sua matricula: ";
					cin >> usuario;
					getchar();
					/* Verifica se o aluno está cadastrado */
					pstmt = con->prepareStatement("SELECT matricula FROM ALUNO WHERE ALUNO.matricula=?");
					pstmt->setInt(1,usuario);
					res = pstmt->executeQuery();
					res->next();
					if(res->rowsCount() == 1) dadosReserva(opcao,usuario,pstmt,driver,con,stmt,res);
					else cout << "Aluno não cadastrado!" << endl;
					break;
				case 2:
					cout << "Digite sua matricula: ";
					cin >> usuario;
					getchar();
					/* Verifica se o professor está cadastrado */
					pstmt = con->prepareStatement("SELECT matricula FROM PROFESSOR WHERE PROFESSOR.matricula=?");
					pstmt->setInt(1,usuario);
					res = pstmt->executeQuery();
					res->next();
					if(res->rowsCount() == 1) dadosReserva(opcao,usuario,pstmt,driver,con,stmt,res);
					else cout << "Professor não cadastrado!" << endl;
					break;

				case 3:
					cout << "Digite seu CPF: ";
					cin >> usuario;
					getchar();
					/* Verifica se o funcionário está cadastrado */
					pstmt = con->prepareStatement("SELECT cpf FROM FUNCIONARIO WHERE FUNCIONARIO.cpf = ?");
					pstmt->setInt(1,usuario);
					res = pstmt->executeQuery();
					if(res->rowsCount() == 1) dadosReserva(opcao,usuario,pstmt,driver,con,stmt,res);
					else cout << "Funcionario não cadastrado!" << endl;
					break;

				default:
					return;
			}
	}
	delete pstmt;
	cout << "Reserva realizada!" << endl;
	} else cout << "Conexao com o banco perdida!" << endl;

}
/** \brief Função responsável por realizar o cadastro de RESERVAS no sistema.*/
void dadosReserva(int opcao,int usuario, PreparedStatement* pstmt, Driver* driver, Connection* con, Statement* stmt, ResultSet* res)
{
	/**
	 \details Cadastra um novo usuario no sistema armazenando seus dados principais
	 \e coletando fotos para seu reconhecimento.
	 \param opcao é a variável que define quem está autenticando a reserva
	 \param int usuario é o cadastro do usuário
	 \param pstmt é a variável responsável por fazer uma Query no mySQL
	 \param driver é a variável responsável por instanciar um driver
	 \param con é responsável pela conexão com o DB
	 \param stmt é responsável por fazer execuções no DB
	 \param res é utilizado para armazenar a resposta à Query
	*/
	string cod_turma = "X",descricao, horario, disciplina="X", dtinit, dtfim, dia;
	int matricula_participante=-1, sala, matricula_funcionario=-1, matricula_professor=-1, matricula_aluno=-1;
	int num_part = 0, num_salas = 0, i = 0, recorrente = 0;

	cout << "Digite a descricao: ";
	getline(cin, descricao);
	cout << "Digite o número de participantes:";
	cin >> num_part;
	while(i<num_part)
	{
		cout << "Digite a matricula do participante " << i+1  << ":";
		cin >> matricula_participante;
		i++;
	}
	cout << "Digite o dia da reserva:";	
	cin >> dia;
	cout << "Digite a hora da reserva:";	
	cin >> horario;

	dtinit = dia;
	dtfim = dia;
	cout << "A Reserva será recorrente?:";
	cout << endl;
	cout << "0 - para Não" << endl;
	cout << "1 - para Sim" << endl;
	cin >> recorrente;
	
	if(recorrente == 1)
	{
		cout << "Digite a data inicial: ";
		cin >> dtinit;
		cout << "Digite a data final: ";
		cin >> dtfim;

	}
	cout << "Quantas Salas serão utilizadas?:";
	cin >> num_salas;
	i=0;
	while(i<num_salas)
	{
		cout << "Digite o numero da Sala a ser reservada: ";
		cin >> sala;
		i++;
	}
	if(opcao == 1) matricula_aluno = usuario;
	else if(opcao == 2)
	{
		matricula_professor = usuario;
		cout << "Digite a disciplina:";
		cin >> disciplina;
		cout << "Digite a turma:";
		cin >> cod_turma;
	}
	else matricula_funcionario = usuario;
	
	/*Escreve o funcionario no DB */
	pstmt = con->prepareStatement("INSERT INTO RESERVA (descricao, nome_disciplina,turma,num_sala, \
					horario,matricula_aluno,matricula_professor, matricula_funcionario,dia,dt_init,dt_fim) \
					VALUES(?,?,?,?,?,?,?,?,?,?,?)");
	pstmt->setString(1,descricao);
	pstmt->setString(2,disciplina);
	pstmt->setString(3,cod_turma);
	pstmt->setInt(4,sala);
	pstmt->setString(5,horario);
	pstmt->setInt(6,matricula_aluno);
	pstmt->setInt(7,matricula_professor);
	pstmt->setInt(8,matricula_funcionario);
	pstmt->setString(9,dia);
	pstmt->setString(10,dtinit);
	pstmt->setString(11,dtfim);
	pstmt->executeUpdate();
}

/** \brief Função responsável por explicitar a reserva feita pelo usuário previamente cadastrado.*/
void mostraReserva(int cadastro, PreparedStatement* pstmt, Driver* driver, Connection* con, Statement* stmt, ResultSet* res ){
	 /*
	 \details Mostra a reserva referente ao codigo do usuário cadastro
	 \param cadastro descreve o código do usuário
	 \param pstmt é a variável responsável por fazer uma Query no mySQL
	 \param driver é a variável responsável por instanciar um driver
	 \param con é responsável pela conexão com o DB
	 \param stmt é responsável por fazer execuções no DB
	 \param res é utilizado para armazenar a resposta à Query
	*/
	
	/* Verifica se quem está fazendo a reserva é um aluno */
	pstmt = con->prepareStatement("SELECT matricula_aluno FROM RESERVA WHERE matricula_aluno = ?");
	pstmt->setInt(1,cadastro);
	res = pstmt->executeQuery();

	if(res->next() && cadastro == res->getInt(1)){
		pstmt = con->prepareStatement("SELECT descricao FROM RESERVA WHERE matricula_aluno = ?");
		pstmt->setInt(1,cadastro);
		res = pstmt->executeQuery();

		if(res->next()) cout << "Descrição: " << res->getString(1) << endl;

		pstmt = con->prepareStatement("SELECT num_sala FROM RESERVA WHERE matricula_aluno = ?");
		pstmt->setInt(1,cadastro);
		res = pstmt->executeQuery();
	
		if(res->next())	cout << "Numero da Sala: " << res->getInt(1) << endl;

		pstmt = con->prepareStatement("SELECT horario FROM RESERVA WHERE matricula_aluno = ?");
		pstmt->setInt(1,cadastro);
		res = pstmt->executeQuery();
	
		if(res->next()) cout << "Horario: " << res->getString(1) << endl;

		return;

	}

	res = pstmt->executeQuery();/* Verifica se quem está fazendo a reserva é um professor */
	pstmt = con->prepareStatement("SELECT matricula_professor FROM RESERVA WHERE matricula_professor = ?");
	pstmt->setInt(1,cadastro);
	res = pstmt->executeQuery();


	if(res->next() && cadastro == res->getInt(1)){
		pstmt = con->prepareStatement("SELECT descricao FROM RESERVA WHERE matricula_professor = ?");
		pstmt->setInt(1,cadastro);
		res = pstmt->executeQuery();
	
		if(res->next()) cout << "Descrição: " << res->getString(1) << endl;
	
		pstmt = con->prepareStatement("SELECT num_sala FROM RESERVA WHERE matricula_professor = ?");
		pstmt->setInt(1,cadastro);
		res = pstmt->executeQuery();
	
		if(res->next()) cout << "Numero da Sala: " << res->getInt(1) << endl;
	
		pstmt = con->prepareStatement("SELECT horario FROM RESERVA WHERE matricula_professor = ?");
		pstmt->setInt(1,cadastro);
		res = pstmt->executeQuery();
	
		if(res->next()) cout << "Horario: " << res->getString(1) << endl;

		pstmt = con->prepareStatement("SELECT DISCIPLINA.nome FROM RESERVA INNER JOIN DISCIPLINA ON RESERVA.nome_disciplina = DISCIPLINA.nome WHERE RESERVA.matricula_professor = ?");
		pstmt->setInt(1,cadastro);
		res = pstmt->executeQuery();	
	
		if(res->next()) cout << "Disciplina: " << res->getString(1) << endl;
				
		pstmt = con->prepareStatement("SELECT DISCIPLINA.turma FROM RESERVA INNER JOIN DISCIPLINA ON RESERVA.nome_disciplina = DISCIPLINA.nome WHERE RESERVA.matricula_professor = ?");
		pstmt->setInt(1,cadastro);
		res = pstmt->executeQuery();	
	
		if(res->next()) cout << "Turma: " << res->getString(1) << endl;

		return;	
	}

	res = pstmt->executeQuery();/* Verifica se quem está fazendo a reserva é um professor */
	pstmt = con->prepareStatement("SELECT matricula_funcionario FROM RESERVA WHERE matricula_funcionario. = ?");
	pstmt->setInt(1,cadastro);
	res = pstmt->executeQuery();

	if(res->next() && cadastro == res->getInt(1)){
		pstmt = con->prepareStatement("SELECT descricao FROM RESERVA WHERE matricula_funcionario = ?");
		pstmt->setInt(1,cadastro);
		res = pstmt->executeQuery();
	
		if(res->next()) cout << "Descrição: " << res->getString(1) << endl;
	
		pstmt = con->prepareStatement("SELECT num_sala FROM RESERVA WHERE matricula_funcionario = ?");
		pstmt->setInt(1,cadastro);
		res = pstmt->executeQuery();
	
		if(res->next()) cout << "Numero da Sala: " << res->getInt(1) << endl;
	
		pstmt = con->prepareStatement("SELECT horario FROM RESERVA WHERE matricula_funcionario = ?");
		pstmt->setInt(1,cadastro);
		res = pstmt->executeQuery();
	
		if(res->next()) cout << "Horario: " << res->getString(1) << endl;
	
		pstmt = con->prepareStatement("SELECT dt_init FROM RESERVA WHERE matricula_funcionario = ?");
		pstmt->setInt(1,cadastro);
		res = pstmt->executeQuery();	
		
		return;	

	}
	delete pstmt;
}
