/**
  \mainpage
  \authors Guilherme da Silva Fontes Lopes & Alex Nascimento Souza
  \date Release: 22/06/2017
*/
//C++ REQUIRED LIBRARIES
#include <iostream>
#include <ostream>
#include <string>
#include <stdlib.h>
//OPENCV REQUIRED LIBRARIES 
#include <opencv2/opencv.hpp>
#include <opencv/highgui.h>
//MYSQL REQUIRED LIBRARIES 
#include "mysql_connection.h"
#include <mysql_driver.h>
#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>
#include <cppconn/prepared_statement.h>
//HEADERS
#include "sistema.hpp"
#include "faces.hpp"
#include <ctime>

using namespace std;
using namespace sql;
using namespace cv;

/** \brief Função principal do sistema de reconhecimento responsável por organizar a interface
 * usuário máquina e retornar todos os dados relevantes.*/
int main(int argc, char** argv)
{
	int opcao = 0, CPF, matricula;
	bool recognition;
	string aluno, professor, funcionario, password, user, string, path, csv_command, sistema;
	Driver *driver;
	Connection *con;
	Statement *stmt;
	PreparedStatement  *prep_stmt;
	ResultSet *res;

	if(argc != 2) {
		usage();
		return -1;
	}	
	path = argv[1];
	driver = get_driver_instance();
	con = driver->connect("tcp://127.0.0.1:3306","root","root1234!");

	//VERIFICA SE A CONEXÃO COM O BANCO FOI ESTABELECIDA
	if(con->isValid()){
		while(opcao != 5){
			cout << "Menu Opções" << endl;
			cout << "1 - Aluno" << endl;
			cout << "2 - Professor" << endl;
			cout << "3 - Funcionario" << endl;
			cout << "4 - Administrador" << endl;
			cout << "5 - Sair" << endl;
			cout << "Digite a opção desejada:" << endl;
			cin >> opcao;
			
			if(opcao > 5 || opcao < 1)
			{
				cout << "Digite uma opcao valida"	 << endl;
				cin >> opcao;
			}

			stmt = con->createStatement();
			stmt->execute("USE face_recognition_db");
			switch(opcao)
			{
				case 1:
					cout << "Digite matricula:";
					cin >> matricula;
					prep_stmt = con->prepareStatement("SELECT senha FROM ALUNO WHERE matricula=?");
					prep_stmt->setInt(1, matricula);
					res = prep_stmt->executeQuery();
					if(res->rowsCount() == 1){
						//USUÁRIO ESTA REGISTRADO NO BANCO DE DADOS, TENTAR RECONHECIMENTO FACIAL	recognition = face_recognition(matricula);
						aluno = "/Aluno/ > faces_aluno.csv";
						path = path + aluno;
						sistema = "cd .. && python create_csv.py " + path;
						cout << sistema << endl;
						const char *mycharp = sistema.c_str();
						system(mycharp);
						recognition = face_recognition(matricula, "../faces_aluno.csv");
						if(recognition == true){
								cout << "Acesso permitido!" << endl;
								mostraReserva(matricula,prep_stmt,driver,con,stmt,res);
						
						} else {
							cout << "Reconhecimento facial falhou!" << endl;
							cout << "Digite a senha: ";
							cin >> password;
							res->next();
							if(password == res->getString(1)){
								cout << "Acesso permitido!" << endl;
								mostraReserva(matricula,prep_stmt,driver,con,stmt,res);
							} else {
								cout << "Senha invalida!"	<< endl;
								cout << "Tente novamente ou fale com o administrador!" << endl;
							}
						}
					} else {
						cout << "MATRICULA NAO ENCONTRADA!" << endl;
						cout << "PARA REGISTRAR NOVO USUARIO FALAR COM ADMINISTRADOR!" << endl;
					}
					break;

				case 2:
					cout << "Digite matricula:";
					cin >> matricula;
					prep_stmt = con->prepareStatement("SELECT senha FROM PROFESSOR WHERE PROFESSOR.matricula=?");
					prep_stmt->setInt(1, matricula);
					res = prep_stmt->executeQuery();
					if(res->rowsCount() == 1){
						//USUÁRIO ESTA REGISTRADO NO BANCO DE DADOS, TENTAR RECONHECIMENTO FACIAL	recognition = face_recognition(matricula);
						professor = "/Professor/ > faces_professor.csv";
						path = path + professor;
						sistema = "cd .. && python create_csv.py " + path;
						cout << sistema << endl;
						const char *mycharp = sistema.c_str();
						system(mycharp);
						recognition = face_recognition(matricula, "../faces_professor.csv");
						if(recognition == true){
								cout << "Acesso permitido!" << endl;
								mostraReserva(matricula,prep_stmt,driver,con,stmt,res);
						
						} else {
							cout << "Reconhecimento facial falhou!" << endl;
							cout << "Digite a senha: ";
							cin >> password;
							res->next();
							if(password == res->getString(1)){
								cout << "Acesso permitido!" << endl;
								mostraReserva(matricula,prep_stmt,driver,con,stmt,res);
							} else {
								cout << "Senha invalida!"	<< endl;
								cout << "Tente novamente ou fale com o administrador!" << endl;
							}
						}
					} else {
						cout << "MATRICULA NAO ENCONTRADA!" << endl;
						cout << "PARA REGISTRAR NOVO USUARIO FALAR COM ADMINISTRADOR!" << endl;
					}
					break;

				case 3:
					cout << "Digite o CPF:";
					cin >> matricula;
					prep_stmt = con->prepareStatement("SELECT senha FROM FUNCIONARIO WHERE FUNCIONARIO.CPF=?");
					prep_stmt->setInt(1, matricula);
					res = prep_stmt->executeQuery();
					if(res->rowsCount() == 1){
						//USUÁRIO ESTA REGISTRADO NO BANCO DE DADOS, TENTAR RECONHECIMENTO FACIAL	recognition = face_recognition(matricula);
						funcionario = "/Funcionario/ > faces_funcionario.csv";
						path = path + funcionario;
						sistema = "cd .. && python create_csv.py " + path;
						cout << sistema << endl;
						const char *mycharp = sistema.c_str();
						system(mycharp);
						recognition = face_recognition(matricula, "../faces_funcionario.csv");
						if(recognition == true){
								cout << "Acesso permitido!" << endl;
								mostraReserva(matricula,prep_stmt,driver,con,stmt,res);
						
						} else {
							cout << "Reconhecimento facial falhou!" << endl;
							cout << "Digite a senha: ";
							cin >> password;
							res->next();
							if(password == res->getString(1)){
								cout << "Acesso permitido!" << endl;
								mostraReserva(matricula,prep_stmt,driver,con,stmt,res);
							} else {
								cout << "Senha invalida!"	<< endl;
								cout << "Tente novamente ou fale com o administrador!" << endl;
							}
						}
					} else {
						cout << "FUNCIONARIO NAO ENCONTRADO!" << endl;
						cout << "PARA REGISTRAR NOVO USUARIO FALAR COM ADMINISTRADOR!" << endl;
					}
					
					break;

				case 4:
					cout << "Digite a senha: ";
					cin >> password;
					prep_stmt = con->prepareStatement("SELECT senha FROM ADMINISTRADOR WHERE ADMINISTRADOR.senha=?");
					prep_stmt->setString(1, password);
					res = prep_stmt->executeQuery();
					res->next();
					if(password == res->getString(1)){
						//OPÇÕES PARA CADASTRO DO ALUNO
						cout << "Menu Cadastro" << endl;
						cout << "1 - Aluno" << endl;
						cout << "2 - Professor" << endl;
						cout << "3 - Funcionario" << endl;
						cout << "4 - Reserva" << endl;
						cout << "5 - Sair" << endl;
						cout << "Digite a opção desejada:" << endl;
						cin >> opcao;
						switch(opcao){
							case 1:
								cadastraUsuario(opcao,prep_stmt,driver,con,stmt,res);
								return 0;
							case 2:
								cadastraUsuario(opcao,prep_stmt,driver,con,stmt,res);
								return 0;
							case 3:
								cadastraUsuario(opcao,prep_stmt,driver,con,stmt,res);
								return 0;
							case 4:
								cadastraReserva(prep_stmt,driver,con,stmt,res);
								break;
							default:
								cout << "OPÇAO DE CADASTRO INCORRETO!" << endl;
						}	
					}
					else cout << "SENHA INCORRETA!" << endl;
					break;

				default:
					return 0;
				
			}	
		} 
	} else {
		cout << "ERRO AO SE CONECTAR AO BANCO DE DADOS!" << endl;
	}
	delete res;
	delete stmt;
	delete con;
	return 0;
}
