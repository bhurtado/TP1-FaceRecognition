/*! @file func.cpp
*	@authors Guilerme Lopes and Alex Souza
*	@brief Esse arquivo descreve as funções de arquivamento de imagens do sistema de reconhecimento facial.
*/

#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <opencv2/opencv.hpp>
#include <iostream>
#include <string>
#include <stdio.h>
#include "faces.hpp"

using namespace std;
using namespace cv;

/** Global variables */
String face_cascade_name = "../haarcascade_frontalface_alt.xml";
CascadeClassifier face_cascade;
RNG rng(12345);

/** \brief Função responsável por tirar as fotos que serão inseridas no banco de dados para uso no reconhecimento facial.*/
void set_camera_print (string matricula, string diretorio){

	/**
		\details Controla o número de fotos que serão tiradas, limitando-as a 10.
	 	\param matricula -> identificador do usuário sendo inserido no banco
	 	\diretorio -> caminho para onde a foto sera armazenada
	*/

  	string image_name;
	VideoCapture *mCamera;
	VideoCapture capture;
	Mat frame;
	int i = 0;

  // Show the image captured from the camera in the window and repeat
	capture.open(0); 
  if (!capture.isOpened()) cerr << "ERROR: capture is NULL" << endl;
  else {
  	cout << "Lets take 10 pictures to do the recognition" << endl;
  	cout << "To take a picture just press c" << endl;
  	// Create a window in which the captured images will be presented
  	while (i < 10) {
  		// Show the image captured from the camera in the window and repeat
    	// Get one frame
			capture >> frame;
    	if (frame.empty()) {
    	    cerr <<  "ERROR: frame is null..." << endl; 
    	    break;
    	} else {
				if( !face_cascade.load(face_cascade_name) ){
					printf("--(!)Error loading\n");
				};
    		detectAndDisplay(frame,matricula,&i,diretorio);
			} 
			//-- Show what you got
			imshow("VideoCapture", frame);
		}
	}
	cout << "Cadastro finalizado com sucesso." << endl;
}
/** \brief Função responsável por detectar o rosto na imagem.*/
void detectAndDisplay( Mat frame, string matricula, int *j, string diretorio)
{

	/**
	 \details Função tem intuito de limitar a imagem que será salva para captar somento o rosto.
	 \10 imagens de cada usuário são salvas para garantir um melhor reconhecimento.
	 \param frame -> quadro sendo analisado
	 \param matricula -> identificador do usuário sendo inserido no banco
	 \j -> indica o número da foto que esta sendo tirada
	 \diretorio -> caminho para onde a foto sera armazenada
	*/
	string image_number[10] ={"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"};
	string image_name;
	vector<Rect> faces;
	Mat frame_gray;
	
	cvtColor( frame, frame_gray, CV_BGR2GRAY );
	equalizeHist( frame_gray, frame_gray );
	
	//-- Detect faces
	face_cascade.detectMultiScale( frame_gray, faces, 1.1, 2, 0|CV_HAAR_SCALE_IMAGE, Size(30, 30) );
	
	for( size_t i = 0; i < faces.size(); i++ )
	{
		Rect face_i = faces[i];
	  Point center( faces[i].x + faces[i].width*0.5, faces[i].y + faces[i].height*0.5 );
	  rectangle(frame, face_i, CV_RGB(0, 255,0), 1);
	
	  Mat faceROI = frame_gray( faces[i] );
  	image_name = "/" + image_number[*j] + ".pgm";
		int c = waitKey(10);
		if( (char)c == 'c' ){
			diretorio = diretorio +  image_name;
			imwrite(diretorio, faceROI);
			cout << "Picture: " << *j+1 << " OK!" << endl;
			*j = *j + 1;
		} else if((char)c == 'q') break;					
	
	}
}
