# Sistema de Controle de Acesso
O Sistema de Controle de Acesso com Reconhecimento Facial é um projeto escrito em C++ no qual simula um sistema de cadastramento de usuários em um Laborátorio qualquer. 
Para esse projeto foi utilizado a biblioteca **OpenCV** para auxiliar no registro das imagens faciais de cada usuário e no reconhecimento facial para acesso às Reservas feitas no Laboratório.

# Instalação
Para a utilização do Sistema de Reconhecimento Facial é necessário o sistema operacional [About OpenCV]. Além disso, é necessária a instalação de três bibliotecas:
1. OpenCV
 - A instalação da OpenCV para Ubuntu pode ser feita por meio do gerenciador de pacotes `apt-get`. De acordo com o site da [OpenCV], são necessários 3 comandos para a instalação da biblioteca:
    - **sudo apt-get install build-essential** 
    - **sudo apt-get install cmake git libgtk2.0-dev pkg-config libavcodec-dev libavformat-dev libswscale-dev**
    - **sudo apt-get install python-dev python-numpy libtbb2 libtbb-dev libjpeg-dev libpng-dev libtiff-dev libjasper-dev libdc1394-22-dev**

2. Boost
 - A biblioteca boost deve ser baixada neste link [Boost]. Sendo assim, na pasta de sua preferência, o comando `tar --bzip2 -xf /path/to/boost_1_64_0.tar.bz2
` deve ser executada para a instalação completa dessa biblioteca. Por fim, é necessário incluir a biblioteca `<boost/filesystem.h>` no programa.

3. Mysql Connector
 - Para a utilização do Mysql Connector é necessário, primeiramente, a instalação do **MySQL Server** com o comando `sudo apt-get install mysql-server`. Uma vez instalado o Mysql Server, o driver de conexão com o banco de dados Mysql pode ser instalado
com o seguinte comando `apt-get install libmysqlcppconn-dev`.

4. Python
 - A instalação do Python é necessária para a utilização do Arquivo CSV (Comma Separated Value) para a organização dos Paths de cada imagem referente aos usuários do sistema. Sendo assim, o seguinte comando deve ser executado na tela do terminal: `sudo apt-get install python2.7` para a instalação da versão 2.7 do python. Outras versões como Python 2.4 ou 3.6 também estão disponíveis para esse comando.

[OpenCV]: <http://docs.opencv.org/2.4>
[Boost]: <http://www.boost.org/users/history/version_1_64_0.html>
[About OpenCV]:<http://opencv.org/about.html>

# Compilação

1. Descrição

Para a utilização do sistema, abra o terminal na pasta onde o projeto será armazenado e digite o seguinte comando: 

- **git clone https://gitlab.com/Guilhermesfl/TP1-FaceRecognition.git**

Feito isso, agora o projeto se encontra no seu computador. Digite então os seguintes comandos no terminal:

- **cd TP1-FaceRecognition**
- **mkdir build**
- **cd build/**
- **cmake ..**
- **make**

O projeto será então compilado. Para executar o programa basta então digitar

- **./face_recognition_system path/to/project/folder**, onde **path/to/project/folder** é o path para a pasta na qual o projeto foi extraído.


2. Passo a passo com imagens

![Inicio](/doc_imgs/inicio.png). 
![Meio](/doc_imgs/cmake.png). 
![Fim](/doc_imgs/make_final.png). 

# Utilização
O Sistema é iniciado com a opção de entrada para Aluno, Professor, Funcionário e Administrador, assim como mostra a figura abaixo:

![Menu1](/doc_imgs/menu1.png). 


Inicialmente, não há nenhum indivíduo cadastrado no sistema e, sendo assim, é necessário acessar o Menu do Administrador para o cadastro da pessoa desejada. Para acessar tal menu, escolha a categoria do Administrador e digite a senha '1'.


![Menu2](/doc_imgs/menuadm.png)


A partir do menu do Administrador, como já informado anteriormente, é possível cadastra a Pessoa deseja além de ser possível cadastrar quaisquer reservas, assim como é mostrado na Imagem do Menu do Administrado
## Cadastro
Para o cadastro, certas informações devem ser apresentadas, tais como:
- Nome;
- Sobrenome;
- Matrícula (Para o caso dos Funcionários, CPF);
- Senha para Acesso;
- Número de Disciplinas*;
    - Nome da(s) Disciplina(s)*;
    - Turma(s) da(s) Disciplina(s)*;

--> É estritamente necessário ressaltar que o Banco, por PADRÃO, já possui uma série de disciplinas (com turmas) já cadastradas. São elas:
    
    APC -> Turmas A e B;
    SD -> Turmas A e B;
    TP1 -> Turma A;
    CRB -> Turma A;
    ID -> Turma A;
    
 Após o preenchimento de todos os campos, o sistema fará um requerimento de uma sessão de fotos tiradas pela WebCam para o usuário assim como mostra a imagem a seguir:
 
 
 ![image1](/doc_imgs/takingpicture.png)
 
 
 Como é possível observar na imagem, o programa tira as fotos depois de pressionada a teclada 'c'. O processo é repetido até que 10 fotos sejam tiradas.
 Após a execução dessa parte do programa, as 10 fotos tiradas são salvas em uma pasta com o número de matrícula (ou CPF) na categoria referente ao indivíduo (i.e `/Aluno/150125972` ou `/Funcionario/04891041542`), assim como mostra a imagem:
 
 
 ![imagem2](/doc_imgs/creatingfolder.png). 
 
 **Após a execução do cadastro o programa será encerrado.**
 
## Reconhecimento e Reserva
Cada usuário do Sistema pode fazer uma reserva por vez no laborátorio (i.e caso o usuário tenha alguma reserva feita, é necessário esperar até que ele cumpra o horário para fazer uma nova reserva). Sendo assim, para cada tipo de usuário, a reserva é feita de maneira diferente, assim como mostra o exemplo de Reserva feito por um aluno:

![imagem3](/doc_imgs/reserva_aluno.png).

Ou por um Professor:

![imagem4](/doc_imgs/reserva_professor.png).

Para o sistema de Reconhecimento Facial, o usuário deve fornecer seu código e observar a câmera para que seja feito o reconhecimento, que será executado por, no máximo, 10 segundos em caso de falha nos momentos anteriores. Em caso de sucesso, o sistema emite uma mensagem de acesso permitido e mostra os detalhes da reserva feita pelo usuário assim como mostram as imagens a seguir:

![imagem5](/doc_imgs/reconhecimento_prof.png).                              ![imagem6](/doc_imgs/reconhecimento_aluno.png).

# Modelo Relacional do Banco de Dados

![imagem6](/doc_imgs/FaceRecoMR.png)

# Diagrama de Sequência

![DiagramaSequencia](/doc_imgs/sistema_reconhecimento.jpg)
