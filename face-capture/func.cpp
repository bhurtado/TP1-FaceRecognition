#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <opencv2/opencv.hpp>
#include <iostream>
#include <string>
#include <stdio.h>
#include "face_screenshot.hpp"

using namespace std;
using namespace cv;

/** Global variables */
String face_cascade_name = "../haarcascade_frontalface_alt.xml";
CascadeClassifier face_cascade;
RNG rng(12345);

/** \brief Função que tira as 10 fotos de cada usuário cadastrado */
void set_camera_print (){
  string matricula,image_name;
	VideoCapture *mCamera;
	VideoCapture capture;
	Mat frame;
	int i = 0;

  cout << "Digite sua Matricula" << endl;
  cin >> matricula;

  // Show the image captured from the camera in the window and repeat
	capture.open(0); //= cvCaptureFromCAM( -1 );
  if (!capture.isOpened() ) cerr << "ERROR: capture is NULL" << endl;
  else {
  	cout << "Lets take 5 pictures to do the recognition" << endl;
  	cout << "To take a picture just press c" << endl;
  	// Create a window in which the captured images will be presented
  	while (i < 5) {
  		// Show the image captured from the camera in the window and repeat
    	// Get one frame
			capture >> frame;
    	if (frame.empty()) {
    	    cerr <<  "ERROR: frame is null..." << endl; 
    	    break;
    	} else {
				if( !face_cascade.load(face_cascade_name) ){
					printf("--(!)Error loading\n");
					
				};
    		detectAndDisplay(frame,matricula,&i);
			} 
			
		}
	}
  destroyWindow("VideoCapture");
}
/** @function detectAndDisplay */
void detectAndDisplay( Mat frame, string matricula, int *j)
{
	string image_number[5] ={"1", "2", "3", "4", "5"};
	string image_name;
	vector<Rect> faces;
	Mat frame_gray;
	
	cvtColor( frame, frame_gray, CV_BGR2GRAY );
	equalizeHist( frame_gray, frame_gray );
	
	//-- Detect faces
	face_cascade.detectMultiScale( frame_gray, faces, 1.1, 2, 0|CV_HAAR_SCALE_IMAGE, Size(30, 30) );
	
	for( size_t i = 0; i < faces.size(); i++ )
	{
		Rect face_i = faces[i];
	  Point center( faces[i].x + faces[i].width*0.5, faces[i].y + faces[i].height*0.5 );
	  rectangle(frame, face_i, CV_RGB(0, 255,0), 1);
	
	  Mat faceROI = frame_gray( faces[i] );
  	image_name = matricula + ".png;" + image_number[*j];
		int c = waitKey(10);
		if( (char)c == 'c' ){
			imwrite(image_name, faceROI);
			cout << "Picture: " << *j+1 << " OK!" << endl;
			*j = *j + 1;
		} else if((char)c == 'q') break;					
	
	}
	//-- Show what you got
	imshow( "Video Capture", frame );
}
